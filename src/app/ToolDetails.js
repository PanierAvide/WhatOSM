import React, {Component} from 'react';
import BookIcon from 'material-ui/svg-icons/av/library-books';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import MediaQuery from 'react-responsive';
import TouchappIcon from 'material-ui/svg-icons/action/touch-app';

const style = {
	logo: {
		maxWidth: '100px',
		maxHeight: '50px',
		verticalAlign: 'middle'
	},
	picture: {
		small: {
			maxHeight: '150px',
			maxWidth: '100%',
			display: 'block',
			margin: '10px auto'
		},
		wide: {
			maxHeight: '200px',
			maxWidth: '200px',
			float: 'left',
			marginTop: '5px',
			marginRight: '20px'
		}
	},
	dialog: {
		small: {
			width: '90%'
		}
	},
	closeButton: {
		width: '36px',
		minWidth: '36px',
		maxWidth: '36px',
		position: 'absolute',
		top: '5px',
		right: '5px'
	}
};

/**
 * ToolDetails is the component for showing a particular tool details.
 */
class ToolDetails extends Component {
	constructor(props, context) {
		super(props, context);
		
		this.state = {
			open: true
		};
	}
	
	open() {
		this.setState({open: true});
	}
	
	close() {
		this.setState({open: false});
	}
	
	render() {
		if(!this.props.tool) {
			return null;
		}
		
		const actions = [
			<FlatButton
				label={I18n.t("Try it")}
				primary={true}
				icon={<TouchappIcon />}
				href={this.props.tool.start_url}
				target="_blank"
			/>,
			<FlatButton
				label={I18n.t("Read doc")}
				secondary={true}
				icon={<BookIcon />}
				href={this.props.tool.doc_url}
				target="_blank"
			/>
		];
		
		const title = (this.props.tool.logo && this.props.tool.logo.length > 0) ? <div><img src={this.props.tool.logo} style={style.logo} /> <span dangerouslySetInnerHTML={{__html: this.props.tool.name}} /></div> : this.props.tool.name;
		
		return (
			<div>
				<MediaQuery maxWidth={768}>
					<Dialog
						title={
							<div>
								{title}
								<FlatButton
									icon={<CloseIcon />}
									onClick={this.close.bind(this)}
									style={style.closeButton}
								/>
							</div>
						}
						actions={actions}
						modal={false}
						open={this.state.open}
						onRequestClose={this.close.bind(this)}
						autoScrollBodyContent={true}
						contentStyle={style.dialog.small}
					>
						<img src={this.props.tool.picture} style={style.picture.small} />
						<p>{I18n.tt(this.props.tool.id+"_d")}</p>
					</Dialog>
				</MediaQuery>
				<MediaQuery minWidth={768}>
					<Dialog
						title={
							<div>
								{title}
								<FlatButton
									icon={<CloseIcon />}
									onClick={this.close.bind(this)}
									style={style.closeButton}
								/>
							</div>
						}
						actions={actions}
						modal={false}
						open={this.state.open}
						onRequestClose={this.close.bind(this)}
						autoScrollBodyContent={true}
					>
						<img src={this.props.tool.picture} style={style.picture.wide} />
						<p>{I18n.tt(this.props.tool.id+"_d")}</p>
					</Dialog>
				</MediaQuery>
			</div>
		);
	}
}

export default ToolDetails;
