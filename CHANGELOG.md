# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## Next version (to be released)


## 0.2.19 - 2022-08-29

### Changed
- Update URL of various tools (Kartaview, Cartovrac, Mapillary...)
- Removed deprecated tools (Jungle Bus App, Pic4Carto)

## 0.2.18 - 2019-10-10

### Changed
- Updated German and Hungarian locales
- Switched HOT URL to https


## 0.2.17 - 2019-10-09

### Changed
- Updated Czech and German locales


## 0.2.16 - 2019-10-07

### Fixed
- Maproulette logo URL was broken


## 0.2.15 - 2019-10-07

### Added
- Czech locale (thanks to trendspotter)


## 0.2.14 - 2019-01-01

### Added
- Tool: CartoVrac (thanks to Antoine Jaury)


## 0.2.13 - 2018-06-08

### Added
- Tools: Parking lanes, RoofMapper, Pic4Review
- Timeout value for HTTP requests when checking tool configurations

### Changed
- Locales translations are know written in a human-readable manner in configuration files
- Links to Which tool for OSM updated (moved to whatosm.pavie.info)


## 0.2.12 - 2018-06-04

### Fixed
- Better handling of fallbacks languages (thanks to Julien Lepiller)


## 0.2.11 - 2018-05-24

### Fixed
- StreetComplete picture link was broken


## 0.2.10 - 2017-11-20

### Added
- Hungarian locale (hu) for tools and user interface


## 0.2.9 - 2017-11-01

### Fixed
- URL for HOT and Pic4Carto logos in tool description


## 0.2.8 - 2017-11-01

### Added
- Tools results also includes tools of fewer duration (e.g. searching minutes retrieve minutes + seconds)


## 0.2.7 - 2017-10-16

### Added
- Spanish locale (es) for tools and user interface


## 0.2.6 - 2017-10-06

### Added
- Dutch locale (nl) for tools and user interface


## 0.2.5 - 2017-10-03

### Added
- Portuguese locale (pt) for tools and user interface


## 0.2.4 - 2017-09-26

### Added
- German locale (de) for tools and user interface


## 0.2.3 - 2017-09-22

### Added
- OpenStreetCam tool
- Script for checking URLs in tool descriptions


## 0.2.2 - 2017-09-22

### Changed
- Name is changed into _Which tool for OSM ?_ due to OSM trademark policy

### Fixed
- Wrong folder for UI i18n generated file


## 0.2.1 - 2017-09-21

### Fixed
- Bug around locales for tools descriptions


## 0.2.0 - 2017-09-21

### Added
- Internationalization system for user interface
- Internationalization system for tools description
- French locale (fr) for tools and user interface
- OSMCha tool


## 0.1.0 - 2017-09-16

### Added
- User interface for finding contribution tools
- 19 contributions tools
- Documentation for using and extending the tool
- Automatic deployment
